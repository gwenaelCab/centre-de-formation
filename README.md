
## Installation 

1. Faire un clone du projet
2. Dans le projet lancer la commandes suivante `npm run is`
Cette commande reviens à faire :
    - `npm install`
    - `npm start`
(utilise le build situé dans front-app/centre-de-formation-front/dist du projet angular)
Le projet est alors accessible sur http://localhost:3000/
Les routes commençant par (http://localhost:3000) /api/ permette d'accéder aux resources backend

### Convention GIT

Dans l'idéal une branche par ticket

##### Nom de branch 
regex : `\d+\/.*`
Exemple : "12/page-de-creation"
12 est le numéro du ticket


##### Message de commit
regex : `(^(feat|fix|build|chore|ci|docs|perf|refactor|revert|style|test)(\/)#\d+(: )[\s\S]*)|(Merge branch[\s\S]*)`
Exemple : "feat/#12: ajout de ..."

: #12 est la réference à l'issue numéro 12 dans gitlab

En cas d'erreur sur le nom du commit git commit --amend -m "New commit message."

-----------------------------------------------------------

### CI/CD 

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.
