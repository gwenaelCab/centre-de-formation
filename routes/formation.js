const fs = require('fs');
var express = require('express');
var router = express.Router();


console.log("parsing formations.json")
let rawdata = fs.readFileSync('data/formations.json');
var formations = JSON.parse(rawdata);

// GET cour
router.get('/:id_form/cour/:id_cour', function (req, res, next) {
    let f = formations.find(u => u.id == req.params.id_form);
    if (f == null) res.status(404).send('Formation Inexistante');

    let c = f.edt.find(u => u.id == req.params.id_cour);
    if (c == null) res.status(404).send('Cour Inexistant');

    res.json(c);
});

// POST cour
router.post('/:id_form/cour/', function (req, res, next) {
    let newCour = req.body;

    let f = formations.find(u => u.id == req.params.id_form);
    if (f == null) res.status(404).send('Formation Inexistante');


    newCour.id = f.edt.length + 1;
    f.edt.push(newCour);
    updateJSON();

    res.send('OK');
});

// PUT cour
router.put('/:id_form/cour/:id_cour', function (req, res, next) {
    let f = formations.find(u => u.id == req.params.id_form);
    if (f == null) res.status(404).send('Formation Inexistante');

    let c = f.edt.find(u => u.id == req.params.id_cour);
    if (c == null) res.status(404).send('Cour Inexistant');

    req.body.id = parseInt(req.params.id_cour);
    f.edt[f.edt.findIndex(u => u.id == req.params.id_cour)]=req.body;
    updateJSON();

    res.send("OK")
});

// DELETE cour
router.delete('/:id_form/cour/:id_cour', function (req, res, next) {
    let f = formations.find(u => u.id == req.params.id_form);
    if (f == null) res.status(404).send('Formation Inexistante');

    let c = f.edt.find(u => u.id == req.params.id_cour);
    if (c == null) res.status(404).send('Cour Inexistant');

    f.edt.splice(f.edt.findIndex(u => u.id == req.params.id_cour), 1);
    updateJSON();

    res.send('OK');
});


// GET All formations
router.get('/', function (req, res, next) {
    res.json(formations);
});

// GET formation
router.get('/:id', function (req, res, next) {
    let fo = formations.find(u => u.id == req.params.id)
    if (fo != null)
        res.json(fo);
    else
        res.status(404).send('Formation Inexistante');

});

// POST formation
router.post('/', function (req, res, next) {
    let newUti = req.body;
    newUti.id = formations.length + 1;
    formations.push(newUti);
    updateJSON();
    res.send('OK');
});

// PUT formation
router.put('/:id', function (req, res, next) {
    let fo = formations.find(u => u.id == req.params.id)
    if (fo != null) {
        req.body.id = parseInt(req.params.id);
        formations[formations.findIndex(e => e.id == req.params.id)] = req.body;
        updateJSON();
        res.send("OK")
    }
    else
        res.status(404).send('Formation Inexistante');

});
// DELETE formation
router.delete('/:id', function (req, res, next) {
    if (formations.some(u => u.id == req.params.id)) {
        formations.splice(formations.findIndex(e => e.id == req.params.id), 1);
        updateJSON();
        res.send('OK');

    } else
        res.status(404).send('Formation Inexistante');

});
function updateJSON() {
    fs.writeFile('data/formations.json', JSON.stringify(formations),
        function (err) {
            if (err) return console.log(err);
            console.log("Formations saved!");
        }
    )
}

module.exports = router;
