const fs = require('fs');
var express = require('express');
var router = express.Router();


console.log("parsing utilisateurs.json")
let rawdata = fs.readFileSync('data/utilisateurs.json');
var utilisateurs = JSON.parse(rawdata);

// GET All utilisateurs
router.get('/', function (req, res, next) {
    console.log(utilisateurs)
    res.json(utilisateurs);
});

// GET utilisateur
router.get('/:id', function (req, res, next) {
    let uti = utilisateurs.find(u => u.id == req.params.id)
    if (uti != null)
        res.json(uti);
    else
        res.status(404).send('Utilisateur Inexistant');

});

// POST utilisateur
router.post('/', function (req, res, next) {
    let newUti = req.body;
    newUti.id = utilisateurs.length + 1;
    utilisateurs.push(newUti);
    updateJSON();
    res.send('OK');
});
// PUT utilisateur
router.put('/:id', function (req, res, next) {
    let uti = utilisateurs.find(u => u.id == req.params.id);
    if (uti != null) {
        req.body.id = parseInt(req.params.id);
        utilisateurs[utilisateurs.findIndex(e => e.id == req.params.id)] = req.body;
        updateJSON();
        res.send("OK");
    } else
        res.status(404).send('Utilisateur Inexistant');
});

// DELETE utilisateur
router.delete('/:id', function (req, res, next) {
    if (utilisateurs.some(u => u.id == req.params.id)) {
        utilisateurs.splice(utilisateurs.findIndex(e => e.id == req.params.id), 1);
        updateJSON();
        res.send('OK');

    } else
        res.status(404).send('Utilisateur Inexistant');

});
function updateJSON() {
    fs.writeFile('data/utilisateurs.json', JSON.stringify(utilisateurs),
        function (err) {
            if (err) return console.log(err);
            console.log("The utilisateur saved!");
        }
    )
}

module.exports = router;
