var data = {
    formations: [
        {
            nom: "L3 Info ",
            edt: [
                {
                    id_enseignement: 12,
                    id_enseignant: 1,
                    date_deb: "2019-05-27T11:53:32.118Z",
                    date_fin: "2019-05-27T11:53:32.118Z",
                    description: "Un cour sympa",
                    type: "COURS", //(COURS, TD, TP),
                    modalite: 'PRESENCE', //(PRESENCE, DISTANCE, PRESENCE_DISTANCE)
                    declarants: [12, 23],
                    presents: [23]
                }
            ]
        }
    ],
    enseignements: [
        {
            id: 12,
            nom: 'DB'
        },
        {
            id: 4,
            nom: "Web"
        }
    ],
    utilisateurs: [
        {
            id: "U3628",
            nom: "Jean",
            prenom: "ValJean",
            formation: 23,
            profil: "ELEVE" // (SECRETARIAT, ELEVE, ENSEIGNANT)

        }
    ]
}