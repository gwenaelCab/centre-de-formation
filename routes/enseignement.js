const fs = require('fs');
var express = require('express');
var router = express.Router();


console.log("parsing enseignements.json")
let rawdata = fs.readFileSync('data/enseignements.json');
var enseignements = JSON.parse(rawdata);

// GET All enseignements
router.get('/', function (req, res, next) {
    console.log(enseignements)
    res.json(enseignements);
});

// GET enseignement
router.get('/:id', function (req, res, next) {
    let ens = enseignements.find(u => u.id == req.params.id)
    if (ens != null)
        res.json(ens);
    else
        res.status(404).send('NOPE');

});
// PUT enseignement
router.put('/:id', function (req, res, next) {
    let ens = enseignements.find(u => u.id == req.params.id)
    if (ens != null) {
        req.body.id = parseInt(req.params.id);
        enseignements[enseignements.findIndex(e => e.id == req.params.id)] = req.body;
        updateJSON()
        res.send("OK");
    } else
        res.status(404).send('NOPE');

});

// POST enseignement
router.post('/', function (req, res, next) {
    let newUti = req.body;
    newUti.id = enseignements.length + 1;
    enseignements.push(newUti);
    updateJSON()
    res.send('OK');
});

// DELETE enseignement
router.delete('/:id', function (req, res, next) {
    if (enseignements.some(u => u.id == req.params.id)) {
        enseignements.splice(enseignements.findIndex(e => e.id == req.params.id), 1);
        updateJSON()
        res.send('OK');
    } else
        res.status(404).send('NOPE');

});
function updateJSON  () {
    fs.writeFile('data/enseignements.json', JSON.stringify(enseignements),
        function (err) {
            if (err) return console.log(err);
            console.log("The utilisateur saved!");
        }
    )
}

module.exports = router;
