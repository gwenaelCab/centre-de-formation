import {CreneauModel} from './creneau.model';

export class FormationModel {
  id: number | undefined;
  nom: string | undefined;
  edt: CreneauModel[] | undefined;

  constructor(id?: number, nom?: string, creneau?: CreneauModel[]) {
    this.id = id;
    this.nom = nom;
    this.edt = creneau;
  }
}
