import {ProfilModel} from './profil.model';

export class UtilisateurModel {
  id: number | undefined;
  nom: string | undefined;
  prenom: string | undefined;
  formation: number | undefined;
  profil: ProfilModel | undefined;

  constructor(id?: number, nom?: string, prenom?: string, formation?: number, profil?: ProfilModel) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.formation = formation;
    this.profil = profil;
  }

  /**
   * with {lastname, firstname} return in format LASTNAME Firstname
   * if user or lastname and firstname is not defined return an empty string
   * @param user a user with a least lastname and firstname defined
   */
  public static getNomPrenom(user: UtilisateurModel): string {
    if (user && user.nom && user.prenom) {
      return user.nom.toUpperCase() + ' ' + user.prenom.charAt(0).toUpperCase() + user.prenom.slice(1);
    }
    return '';
  }
}
