export enum ModaliteModel {
  PRESENCE = 'Presence',
  DISTANCE = 'Distance',
  PRESENCE_DISTANCE = 'Presence et Distance'
}
