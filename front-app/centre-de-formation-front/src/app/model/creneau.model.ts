import {TypeCourModel} from './typeCour.model';
import {ModaliteModel} from './modalite.model';

export class CreneauModel {
  id: number | undefined;
  idEnseignement: number | undefined;
  idEnseignant: number | undefined;
  dateDeb: Date | undefined;
  dateFin: Date | undefined;
  description: string | undefined;
  type: TypeCourModel | undefined;
  modalite: ModaliteModel | undefined;
  declarants: number[] | undefined = [];
  presents: number[] | undefined = [];

  constructor(id?: number, idEnseignement?: number, idEnseignant?: number,
              dateDeb?: Date, dateFin?: Date, description?: string, type?: TypeCourModel,
              modalite?: ModaliteModel, declarants?: number[], presents?: number[]) {
    this.id = id;
    this.idEnseignement = idEnseignement;
    this.idEnseignant = idEnseignant;
    this.dateDeb = new Date(dateDeb as Date);
    this.dateFin = new Date(dateFin as Date);
    this.description = description;
    this.type = type;
    this.modalite = modalite;
    this.declarants = declarants ? declarants : [];
    this.presents = presents ? presents : [];
  }
}
