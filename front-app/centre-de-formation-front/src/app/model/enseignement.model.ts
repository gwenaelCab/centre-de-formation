export class EnseignementModel {
  id: number | undefined;
  nom: string | undefined;
  color: string | undefined;

  constructor(id?: number, nom?: string, color?: string) {
    this.id = id;
    this.nom = nom;
    this.color = color;
  }
}
