import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CoreModule} from './core/core.module';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {LoginPageComponent} from './login-page/login-page.component';
import {NgSelectModule} from "@ng-select/ng-select";
import {DateAdapter, MAT_DATE_LOCALE} from "@angular/material/core";
import {FrenchDateAdapter} from "./core/adapters/french-date-adapter";


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    AppComponent,
  ],
  imports: [
    CoreModule,
    NgxMaterialTimepickerModule,
    AppRoutingModule,
    BrowserModule,
    FlexLayoutModule,
    NgSelectModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {provide: DateAdapter, useClass: FrenchDateAdapter},
    MatDatepickerModule
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
