import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PlanningPageComponent} from './planning-page.component';
import {MatDialog} from '@angular/material/dialog';
import {OverlayModule} from '@angular/cdk/overlay';

describe('planningPagetComponent', () => {
  let component: PlanningPageComponent;
  let fixture: ComponentFixture<PlanningPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PlanningPageComponent],
      providers: [MatDialog, OverlayModule]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
