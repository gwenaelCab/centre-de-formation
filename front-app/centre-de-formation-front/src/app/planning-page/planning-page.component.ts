import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {NotificationService} from '../core/services/notification.service';
import {Title} from '@angular/platform-browser';
import {PopupAddComponent} from './popup-add/popup-add.component';
import {UtilisateurModel} from '../model/utilisateur.model';
import {ProfilModel} from '../model/profil.model';
import {UserService} from '../core/services/user.service';
import {CourseService} from '../core/services/course.service';
import {TrainingService} from '../core/services/training.service';
import {EnseignementModel} from '../model/enseignement.model';
import {FormationModel} from '../model/formation.model';
import {CreneauModel} from '../model/creneau.model';
import * as moment from 'moment';
import {Observable} from 'rxjs';
import {CreneauService} from '../core/services/creneau.service';
import {PopupAppelComponent} from './popup-appel/popup-appel.component';
import {FormControl} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {NgSelectConfig} from '@ng-select/ng-select';
import {AuthService} from '../core/services/auth.service';
import {TypeCourModel} from '../model/typeCour.model';

export enum TypeView {
  WEEK = 'week',
  LIST = 'list'
}

@Component({
  selector: 'app-planning-page',
  templateUrl: './planning-page.component.html',
  styleUrls: ['./planning-page.component.scss']
})

export class PlanningPageComponent implements OnInit {
  eTypeView = TypeView;
  teachers: UtilisateurModel[] = [];
  students: UtilisateurModel[] = [];
  courses: EnseignementModel[] = [];
  trainings: FormationModel[] = [];
  classEvents: CreneauModel[] = [];
  training: FormationModel | undefined;
  selectedWeek = moment();
  viewType = TypeView.WEEK;

  myControlTraining: FormControl = new FormControl();
  displayedTrainingId?: number | undefined;
  filteredOptionsTraining?: Observable<FormationModel[]>;
  optionsTraining: FormationModel[] = [];

  @Input() hideArrow = this.authService.isStudent();
  typesClassEvents = Object.values(TypeCourModel);
  allTypesClassEvents = this.typesClassEvents.join(' - ');
  selectedTypeClassEvents?: string;
  filterStartDate: any = Date();
  filterEndDate: any;


  constructor(public dialog: MatDialog,
              private readonly notificationService: NotificationService, title: Title,
              private readonly userService: UserService,
              private readonly courseService: CourseService,
              private readonly trainingService: TrainingService,
              private readonly creneauService: CreneauService,
              private config: NgSelectConfig,
              public authService: AuthService) {
    title.setTitle('Planning');
    this.getCourses();
    this.getTeacher();
    this.getStudent();
    this.loadPlanning();
    this.config.notFoundText = 'Pas de formation trouvée';
  }

  ngOnInit(): void {
    this.filteredOptionsTraining = this.myControlTraining.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterTraining(value))
      );

  }


  ajoutCreneau(classEvent?: CreneauModel): void {
    this.ajoutCreneauModal(classEvent).subscribe(creneau => {
      if (creneau) {
        if (creneau.id === undefined) {
          // @ts-ignore
          this.creneauService.insertCreneau(this.training.id, creneau).subscribe(() => {
            this.loadPlanning();
          });
        } else {
          // @ts-ignore
          this.creneauService.updateCreneau(this.training.id, creneau).subscribe(() => {
            this.loadPlanning();
          });
        }
      }
    });
  }

  /**
   * edit presents of classEvent
   * @param classEvent of presents
   */
  faireAppelCreneau(classEvent: CreneauModel): void {
    this.faireAppelCreneauModal(classEvent).subscribe(creneau => {
      if (creneau) {
        // @ts-ignore
        this.creneauService.updateCreneau(this.training.id, creneau).subscribe(() => {
          this.loadPlanning();
        });
      }
    });
  }

  /**
   * ajout ou modification d'un créneau
   */
  ajoutCreneauModal(classEvent?: CreneauModel): Observable<any | undefined> {
    const dialogRef = this.dialog.open(PopupAddComponent,
      {
        width: '350px', height: '500px',
        data: {
          teachers: this.teachers,
          courses: this.courses,
          classEvent
        }
      });
    return dialogRef.afterClosed();
  }

  displayOptionsTraining(training: FormationModel): string {
    return training && training.nom ? training.nom : '';
  }

  /**
   * update training displayed
   * @param selectedTraining training selected by the user in the select
   */
  selectedTrainingChange(selectedTraining: FormationModel): void {
    this.training = selectedTraining;
    this.setClassEvents();
  }

  /**
   * blur input
   */
  blur(): void {
    (document.activeElement as HTMLElement).blur();
  }

  /**
   * set the default selected training
   * only one possible for student
   * and for other role the first training is seleted
   */
  setDefaultSelectedTraining(): void {
    if (this.authService.isStudent()) {
      // @ts-ignore
      this.training = this.trainings.find(t => t.id === this.authService.getUser()?.formation);
    } else {
      this.training = this.trainings[0];
    }
    this.myControlTraining.setValue(this.training);
    this.displayedTrainingId = this.training?.id;
  }

  /**
   * build the list of classEvent to display from the training
   * filter by classEvent type if selected
   * if typeView LIST apply date filter
   */
  public setClassEvents(): void {
    let classEvents: any = [];
    if (this.training?.nom) {
      classEvents = this.trainings.find(value => value.nom === this.training?.nom)?.edt;
    } else {
      this.trainings.forEach(value => classEvents = classEvents.concat(value.edt));
    }
    this.classEvents = classEvents ? classEvents : [];
    if (this.selectedTypeClassEvents && this.selectedTypeClassEvents !== this.allTypesClassEvents) {
      this.classEvents = this.classEvents.filter(value => value.type
        === this.selectedTypeClassEvents?.toUpperCase());
    }
    if (this.viewType === this.eTypeView.LIST) {
      if (this.filterStartDate) {
        this.classEvents = this.classEvents.filter(value => {
          return moment(value.dateDeb).isAfter(moment(this.filterStartDate).startOf('d'));
        });
      }
      if (this.filterEndDate) {
        this.classEvents = this.classEvents.filter(value => {
          return moment(value.dateFin).isBefore(moment(this.filterEndDate).endOf('day'));
        });
      }
    }
  }

  /**
   * remove focus of date range if date selected
   */
  removeFocus(): void {
    document.getElementById('fieldTypeClassEvents')?.classList.remove('mat-focused');
  }

  /**
   * ajout ou modification de l'appel d'un créneau
   */
  private faireAppelCreneauModal(classEvent: CreneauModel): Observable<any | undefined> {
    const dialogRef = this.dialog.open(PopupAppelComponent,
      {
        width: '350px', height: '500px',
        data: {
          students: this.students,
          classEvent
        }
      });
    return dialogRef.afterClosed();
  }

  /**
   * get teachers from back
   * @private
   */
  private getTeacher(): void {
    this.userService.getUsersWithProfile(ProfilModel.ENSEIGNANT).subscribe(value => {
      this.teachers = value;
    });
  }

  /**
   * get students from back
   * @private
   */
  private getStudent(): void {
    this.userService.getUsersWithProfile(ProfilModel.ELEVE).subscribe(value => {
      this.students = value;
    });
  }

  /**
   * get all classEvents
   * @private
   */
  private getCourses(): void {
    this.courseService.getAllCourses().subscribe(value => {
      this.courses = value;
    });
  }

  /**
   * get all trainings
   * @private
   */
  private getTraining(): void {
    this.trainingService.getAllTrainings().subscribe((trainings: FormationModel[]) => {
      this.trainings = trainings;
      this.optionsTraining = trainings;
      this.setDefaultSelectedTraining();
      this.setClassEvents();
    });
  }

  /**
   * filter traing by search
   * @param value search value
   * @private
   */
  private _filterTraining(value: string): FormationModel[] {
    const filterValue = value.toString().toLowerCase();
    return this.optionsTraining.filter(option => {
      return option && option.nom && option.nom.toLowerCase().includes(filterValue);
    });
  }

  private loadPlanning(): void {
    this.getTraining();
  }

  deleteCreneau(classEvent: CreneauModel){
      if (this.training && this.training.id != null && classEvent.id != null) {
        this.creneauService.deleteCreneau(this.training.id, classEvent.id).subscribe();
        const index = this.classEvents.indexOf(classEvent);
        if (index > -1) {
          const varTemp = this.classEvents.slice();
          varTemp.splice(index, 1);
          this.classEvents = varTemp;
        }

      }
  }
}
