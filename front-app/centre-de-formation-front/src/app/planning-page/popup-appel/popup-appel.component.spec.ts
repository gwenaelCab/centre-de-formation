import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupAppelComponent } from './popup-appel.component';

describe('PopupAppelComponent', () => {
  let component: PopupAppelComponent;
  let fixture: ComponentFixture<PopupAppelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PopupAppelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupAppelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
