import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {CreneauModel} from '../../model/creneau.model';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-popup-appel',
  templateUrl: './popup-appel.component.html',
  styleUrls: ['./popup-appel.component.scss']
})
export class PopupAppelComponent implements OnInit {

  listeEtudiant: UtilisateurModel[] = [];
  listeEtudiantInscrit: EtudiantPresent[] = [];
  dataSource: any;
  private classEvent: CreneauModel;

  constructor(
    public dialogRef: MatDialogRef<PopupAppelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.listeEtudiant = data.students;
    this.classEvent = data.classEvent;
    this.checkclassEvent();
  }

  ngOnInit(): void {
  }

  checkclassEvent(): void {
    if (!this.classEvent) {
      this.classEvent = new CreneauModel();
    } else {
      this.classEvent.declarants = this.classEvent.declarants ? this.classEvent.declarants : [];
      this.classEvent.presents = this.classEvent.presents ? this.classEvent.presents : [];
      // @ts-ignore
      for (const idEtudiant of this.classEvent?.declarants) {
        // tslint:disable-next-line:triple-equals
        this.listeEtudiantInscrit.push(new EtudiantPresent(
          this.listeEtudiant.find((e: UtilisateurModel) => e.id == idEtudiant) as UtilisateurModel,
          this.classEvent.presents?.includes(idEtudiant)
        ));
      }
      this.dataSource = new MatTableDataSource<EtudiantPresent>(this.listeEtudiantInscrit);
      console.log(this.listeEtudiant);
      console.log(this.listeEtudiantInscrit);
    }
  }

  onSubmit(): void {
    this.classEvent.presents = [];
    for (const etudiant of this.listeEtudiantInscrit) {
      if (etudiant.etu.id != null && etudiant.isPresent) {
        this.classEvent.presents.push(etudiant.etu.id);
      }
    }
    this.dialogRef.close(this.classEvent);
  }

  cancel(): void {
    this.dialogRef.close(false);
  }
}

export class EtudiantPresent {
  etu: UtilisateurModel;
  isPresent: boolean | undefined = false;

  constructor(etu: UtilisateurModel, isPresent?: boolean) {
    this.etu = etu;
    this.isPresent = isPresent;
  }
}
