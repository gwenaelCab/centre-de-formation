import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CreneauModel} from '../../model/creneau.model';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {TypeCourModel} from '../../model/typeCour.model';
import {ModaliteModel} from '../../model/modalite.model';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {EnseignementModel} from '../../model/enseignement.model';
import {TrainingService} from '../../core/services/training.service';
import * as moment from 'moment';


@Component({
  selector: 'app-popup-add',
  templateUrl: './popup-add.component.html',
  styleUrls: ['./popup-add.component.scss']
})

export class PopupAddComponent implements OnInit, AfterViewInit {

  myControlEnseignant = new FormControl();
  myControlEnseignement = new FormControl();
  optionsEnseignant: UtilisateurModel[] = [];
  optionsEnseignement: EnseignementModel[] = [];

  filteredOptionsEnseignant: Observable<UtilisateurModel[]> | undefined;
  filteredOptionsEnseignement: Observable<EnseignementModel[]> | undefined;

  conditionalOperatorTypeCours = TypeCourModel;
  enumTypeCours = [];

  conditionalOperatorModal = ModaliteModel;
  enumModal = [];

  date: any;
  enseignant: any;
  enseignement: any;
  typeCours: any;
  modalite: any;
  private classEvent: CreneauModel = new CreneauModel();

  beginTime: string = '';
  endTime: string = '';

  constructor(
    public dialogRef: MatDialogRef<PopupAddComponent>,
    private readonly trainingService: TrainingService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.optionsEnseignant = data.teachers;
    this.optionsEnseignement = data.courses;
    this.classEvent = data.classEvent;
    this.checkclassEvent();
  }

  ngAfterViewInit(): void {
    this.checkclassEvent();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.filteredOptionsEnseignant = this.myControlEnseignant.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterEnseignant(value))
      );
    this.filteredOptionsEnseignement = this.myControlEnseignement.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterEnseignement(value))
      );
    // @ts-ignore
    this.enumTypeCours = Object.keys(this.conditionalOperatorTypeCours);
    // @ts-ignore
    this.enumModal = Object.keys(this.conditionalOperatorModal);
  }

  checkclassEvent(): void {
    if (!this.classEvent) {
      this.classEvent = new CreneauModel();
    } else {
      this.date = this.classEvent.dateDeb;

      // @ts-ignore
      if(!isNaN(this.classEvent.dateDeb.getTime()) && !isNaN(this.classEvent.dateFin.getTime())) {
        this.endTime = moment(this.classEvent.dateFin).format('HH:mm');
        this.beginTime = moment(this.classEvent.dateDeb).format('HH:mm');
      }



      this.enseignant = this.optionsEnseignant.find(value => this.classEvent.idEnseignant === value.id);
      this.enseignement = this.optionsEnseignement.find(value => this.classEvent.idEnseignement === value.id);
      this.typeCours = Object.keys(TypeCourModel).find(value => this.classEvent.type === value);
      this.modalite = Object.keys(ModaliteModel).find(value => this.classEvent.modalite === value);
    }
  }

  displayOptionsEnseignant(optionsEnseignant: UtilisateurModel): string {
    return UtilisateurModel.getNomPrenom(optionsEnseignant);
  }

  displayOptionsEnseignement(option: EnseignementModel): string {
    return option && option.nom ? option.nom : '';
  }

  onSubmit(): void {
    this.date.setHours((this.beginTime as string).slice(0, 2) as unknown as number,
      ((this.beginTime as string).slice(3, 5)) as unknown as number);
    this.classEvent.dateDeb = this.date.toISOString();
    this.date.setHours((this.endTime as string).slice(0, 2) as unknown as number,
      ((this.endTime as string).slice(3, 5)) as unknown as number);
    this.classEvent.dateFin = this.date.toISOString();
    this.classEvent.idEnseignant = this.enseignant.id;
    this.classEvent.idEnseignement = this.enseignement.id;
    this.classEvent.type = this.typeCours;
    this.classEvent.modalite = this.modalite;
    this.dialogRef.close(this.classEvent);
  }

  private _filterEnseignant(value: string): UtilisateurModel[] {
    return this.optionsEnseignant.filter(user => UtilisateurModel.getNomPrenom(user).toLowerCase()
      .includes(value.toString().toLowerCase()));
  }

  private _filterEnseignement(value: string): EnseignementModel[] {
    return this.optionsEnseignement.filter(course => (course.nom ? course.nom : '').toLowerCase()
      .includes(value.toString().toLowerCase()));
  }

  beginTimeChange($event: string): void {
    this.beginTime = $event;
  }

  endTimeChange($event: string): void {
    this.endTime = $event;
  }

  isTimeValid(): boolean {
    if (this.beginTime && this.endTime) {
      const hBegin: number = (this.beginTime as string).slice(0, 2) as unknown as number;
      const hEnd: number = (this.endTime as string).slice(0, 2) as unknown as number;
      const mBegin: number = ((this.beginTime as string).slice(3, 5)) as unknown as number;
      const mEnd: number = ((this.endTime as string).slice(3, 5)) as unknown as number;
      return (hBegin === hEnd && mBegin < mEnd) || (hBegin < hEnd);
    }
    return true;
  }

  cancel(): void {
    this.dialogRef.close(false);
  }


}
