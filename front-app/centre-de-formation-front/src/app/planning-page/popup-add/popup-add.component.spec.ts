import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PopupAddComponent} from './popup-add.component';
import {MatDialog} from '@angular/material/dialog';

describe('PopupAddComponent', () => {
  let component: PopupAddComponent;
  let fixture: ComponentFixture<PopupAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PopupAddComponent],
      providers: [MatDialog]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
