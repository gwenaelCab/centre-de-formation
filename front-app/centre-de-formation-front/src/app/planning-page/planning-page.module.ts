import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanningPageComponent} from './planning-page.component';
import {PlanningModule} from '../shared/planning.module';
import {PopupAddComponent} from './popup-add/popup-add.component';
import {MatDialogRef} from '@angular/material/dialog';
import {MatNativeDateModule} from '@angular/material/core';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {SharedModule} from '../shared/shared/shared.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {PopupAppelComponent} from './popup-appel/popup-appel.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {FlexModule} from '@angular/flex-layout';

@NgModule({
  declarations: [PlanningPageComponent, PopupAddComponent, PopupAppelComponent],
  imports: [
    CommonModule,
    SharedModule,
    PlanningModule,
    NgxMaterialTimepickerModule,
    MatNativeDateModule,
    NgSelectModule,
    MatNativeDateModule,
    MatIconModule,
    MatButtonToggleModule,
    FlexModule
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    }
  ]
})
export class PlanningPageModule {
}
