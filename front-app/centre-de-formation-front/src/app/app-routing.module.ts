import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from './core/components/not-found/not-found.component';
import {PlanningPageComponent} from './planning-page/planning-page.component';
import {PlanningPageModule} from './planning-page/planning-page.module';
import {TrainingPageComponent} from './trainings-page/training-page.component';
import {TrainingsPageModule} from './trainings-page/trainings-page.module';
import {LoginPageComponent} from './login-page/login-page.component';
import {MainComponent} from './core/components/main-component/main.component';
import {CoreModule} from './core/core.module';
import {AuthenticatedInGuard} from './core/guards/authentificated-in-guard.service';

const routes: Routes = [
  {path: 'login', component: LoginPageComponent},
  {
    path: '', component: MainComponent, canActivate: [AuthenticatedInGuard],
    children: [
      {path: '', component: PlanningPageComponent},
      {path: 'formation', component: TrainingPageComponent, pathMatch: 'full'},
      {path: '404', component: NotFoundComponent},
      {path: '**', redirectTo: '/404'}
    ]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CoreModule, PlanningPageModule, TrainingsPageModule],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
