import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormationModel} from '../../model/formation.model';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiRoutes} from '../enums/apiRoutes';

@Injectable({
  providedIn: 'root'
})
export class TrainingService {

  constructor(private http: HttpClient) {
  }

  /**
   * get a training by id
   * @param id of training
   */
  public getTraining(id: number): Observable<FormationModel> {
    return this.http.get<FormationModel>(environment.apiUrl + ApiRoutes.TRAINING + id);
  }

  /**
   * insert a training in backend
   * @param formationModel the training to insert
   */
  public insertTraining(formationModel: FormationModel): Observable<ArrayBuffer> {
    // @ts-ignore
    return this.http.post<string>(environment.apiUrl + ApiRoutes.TRAINING, formationModel, {responseType: 'text'});
  }

  /**
   * get all training from backend
   */
  public getAllTrainings(): Observable<FormationModel[]> {
    return this.http.get<FormationModel[]>(environment.apiUrl + ApiRoutes.TRAINING);
  }

  /**
   * delete a training
   * @param id of training
   */
  public deleteTraining(id: number): Observable<ArrayBuffer> {
    // @ts-ignore
    return this.http.delete<string>(environment.apiUrl + ApiRoutes.TRAINING + '/' + id, {responseType: 'text'});
  }
}
