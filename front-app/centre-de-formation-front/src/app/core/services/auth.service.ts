import {Injectable} from '@angular/core';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {UserService} from './user.service';
import {Router} from "@angular/router";
import {ProfilModel} from "../../model/profil.model";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * ---------------------------
   *    NOTHING IS SECURE !
   * ---------------------------
   * If we wanted something secured, we could have use a json web token authentification
   * also known as JWT authentification
   */

  loggedUser: UtilisateurModel | undefined;

  constructor(private userService: UserService, private router: Router) {
  }

  /**
   * return true if user is login
   */
  isLogin(): boolean {
    return this.loggedUser !== undefined;
  }

  /**
   * login a user
   * check if the user pass exist and is the exact same in backend
   * if login successfully redirect to /
   * @param user to login
   */
  login(user: UtilisateurModel): void {
    this.userService.getAllUsers().subscribe(users => {
      const backendUser = users.find(u => u.id === user.id);
      if (JSON.stringify(user) === JSON.stringify(backendUser)) {
        this.loggedUser = backendUser;
        this.router.navigate(['']);
      }
    });

  }

  /**
   * logout the user and redirect to /login
   */
  logout(): void {
    this.loggedUser = undefined;
    this.router.navigate(['login']);
  }

  /**
   * get logged user
   */
  getUser(): UtilisateurModel {
    return this.loggedUser as UtilisateurModel;
  }

  /**
   * return true if user is a student else false
   */
  isStudent(): boolean {
    return this.isLogin() && this.loggedUser?.profil === ProfilModel.ELEVE;
  }

  /**
   * return true if user is a teacher else false
   */
  isTeacher(): boolean {
    return this.isLogin() && this.loggedUser?.profil === ProfilModel.ENSEIGNANT;
  }

  /**
   * return true if user is a administration else false
   */
  isAdmin(): boolean {
    return this.isLogin() && this.loggedUser?.profil === ProfilModel.SECRETARIAT;
  }
}
