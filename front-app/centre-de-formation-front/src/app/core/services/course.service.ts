import {Injectable} from '@angular/core';
import {EnseignementModel} from '../../model/enseignement.model';
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";
import {ApiRoutes} from "../enums/apiRoutes";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) {
  }

  public getCourses(id: number): Observable<EnseignementModel> {
    return this.http.get<EnseignementModel>(environment.apiUrl + ApiRoutes.COURSE + id);
  }

  public insertCourses(enseignementModel: EnseignementModel): Observable<string> {
    return this.http.post<string>(environment.apiUrl + ApiRoutes.COURSE, enseignementModel);
  }

  public getAllCourses(): Observable<EnseignementModel[]> {
    return this.http.get<EnseignementModel[]>(environment.apiUrl + ApiRoutes.COURSE);
  }

}
