import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {ApiRoutes} from '../enums/apiRoutes';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {ProfilModel} from '../../model/profil.model';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  /**
   * delete a user
   * @param id of user
   */
  public deleteUser(id: number): Observable<UtilisateurModel> {
    return this.http.delete<UtilisateurModel>(environment.apiUrl + ApiRoutes.USER + id);
  }

  /**
   * get all users
   */
  public getAllUsers(): Observable<UtilisateurModel[]> {
    return this.http.get<UtilisateurModel[]>(environment.apiUrl + ApiRoutes.USER);
  }

  /**
   * get a user by its profile
   * @param profile of user
   */
  public getUsersWithProfile(profile: ProfilModel): Observable<UtilisateurModel[]> {
    return this.getAllUsers().pipe(map(users => users.filter(user => user.profil === profile)));
  }
}
