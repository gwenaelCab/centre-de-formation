import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FormationModel} from '../../model/formation.model';
import {environment} from '../../../environments/environment';
import {ApiRoutes} from '../enums/apiRoutes';
import {CreneauModel} from '../../model/creneau.model';

@Injectable({
  providedIn: 'root'
})
export class CreneauService {

  constructor(private http: HttpClient) {
  }

  /**
   * get a training with all it ClassEvents in it
   * @param id of training
   */
  public getCreneau(id: number): Observable<FormationModel> {
    return this.http.get<FormationModel>(environment.apiUrl + ApiRoutes.TRAINING + id);
  }

  /**
   * Insert a classEvent in the training
   * @param idFormation id training
   * @param creneau the classEvent to be insertded
   */
  public insertCreneau(idFormation: number, creneau: CreneauModel): Observable<ArrayBuffer> {
    // @ts-ignore
    return this.http.post<string>(environment.apiUrl + ApiRoutes.TRAINING + idFormation + ApiRoutes.CLASSEVENT, creneau, {responseType: 'text'});
  }

  /**
   * get all trainings with all classEvents
   */
  public getAllCreneaus(): Observable<FormationModel[]> {
    return this.http.get<FormationModel[]>(environment.apiUrl + ApiRoutes.TRAINING);
  }

  /**
   * delete a classEvent
   * @param id of classEvent
   */
  public deleteCreneau(idFormation: number, id: number): Observable<ArrayBuffer> {
    // @ts-ignore
    return this.http.delete<string>(environment.apiUrl + ApiRoutes.TRAINING + idFormation + ApiRoutes.CLASSEVENT + id, {responseType: 'text'});
  }

  /**
   * update a classEvent
   * @param idFormation if of trainning
   * @param creneau the new classEvent
   */
  public updateCreneau(idFormation: number, creneau: CreneauModel): Observable<ArrayBuffer> {
    // @ts-ignore
    // tslint:disable-next-line:max-line-length
    return this.http.put<string>(environment.apiUrl + ApiRoutes.TRAINING + idFormation + ApiRoutes.CLASSEVENT + creneau.id, creneau, {responseType: 'text'});
  }
}
