import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  active = false;
  message = '';
  type = '';

  constructor() {
  }

  /**
   * display an error message at the top right of the page for 10 seconds
   * @param message error to display
   */
  error(message?: string): void {
    (async () => {
      this.message = message ? message : 'Une erreur est survenue, Veuillez essayer à nouveau.';
      this.active = true;
      this.type = 'error';
      await new Promise(resolve => setTimeout(resolve, 10000));
      this.active = false;
      this.message = '';
      this.type = '';
    })();
  }
}
