import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {RouterModule} from '@angular/router';
import {NotificationComponent} from './components/notification/notification.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {SharedModule} from '../shared/shared/shared.module';
import {MainComponent} from './components/main-component/main.component';
import {AuthenticatedInGuard} from "./guards/authentificated-in-guard.service";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";


@NgModule({
  providers: [AuthenticatedInGuard],
  imports: [
    HttpClientModule,
    CommonModule,
    SharedModule,
    RouterModule,
    MatIconModule,
    MatTooltipModule
  ],
  exports: [
    SharedModule,
    SidebarComponent,
    NotificationComponent,
    MainComponent
  ],
  declarations: [SidebarComponent, NotificationComponent, NotFoundComponent, MainComponent]
})
export class CoreModule {
}
