import {Component, Input, OnInit} from '@angular/core';
import {NotificationService} from "../../services/notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() message = '';
  @Input() type = '';

  constructor(private notificationService: NotificationService) {
    this.message = notificationService.message;
    this.type = notificationService.type;
  }

  ngOnInit(): void {
  }

}
