import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  getActive(url: string, exact: boolean = false): string {
    if (exact) {
      return this.router.url === url ? 'active' : '';
    }
    return this.router.url.startsWith(url) ? 'active' : '';
  }
}
