import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {NotificationService} from '../../services/notification.service';
import {AuthService} from "../../services/auth.service";
import {UtilisateurModel} from "../../../model/utilisateur.model";

@Component({
  selector: 'app-main-component',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  user?: UtilisateurModel;

  constructor(public title: Title, public notificationService: NotificationService, public authService: AuthService) {
    this.user = authService.getUser();
  }

  ngOnInit(): void {
  }

}
