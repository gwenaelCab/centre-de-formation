import {TestBed} from '@angular/core/testing';

import {AuthenticatedInGuard} from './authentificated-in-guard.service';

describe('AuthentificatedInGuardGuard', () => {
  let guard: AuthenticatedInGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthenticatedInGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
