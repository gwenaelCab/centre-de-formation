import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedInGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {

  }

  /**
   * return true if user can access the route
   * It can if he is login
   * @param route to activate
   * @param state of router
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.authService.isLogin()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

}
