export enum ApiRoutes {
  USER = '/utilisateur/',
  TRAINING = '/formation/',
  COURSE = '/enseignement/',
  CLASSEVENT = '/cour/'
}
