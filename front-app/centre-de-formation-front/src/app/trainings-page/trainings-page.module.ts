import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrainingPageComponent} from './training-page.component';
import {SharedModule} from '../shared/shared/shared.module';


@NgModule({
  declarations: [TrainingPageComponent],
  exports: [TrainingPageComponent],
  imports: [
    SharedModule,
    CommonModule
  ],
})
export class TrainingsPageModule {
}
