import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TrainingService} from '../core/services/training.service';
import {FormationModel} from '../model/formation.model';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {MatSort} from '@angular/material/sort';
import {Title} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {PopupCreateUpdateTrainingComponent} from '../shared/planning/popup-create-update-training/popup-create-update-training.component';
import {NotificationService} from "../core/services/notification.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-training-page',
  templateUrl: './training-page.component.html',
  styleUrls: ['./training-page.component.scss']
})
export class TrainingPageComponent implements OnInit, AfterViewInit {

  trainings = new MatTableDataSource<FormationModel>([]);
  selectedTrainings = new SelectionModel<FormationModel>(true, []);
  displayedColumns: string[] = ['select', 'nom'];
  @ViewChild(MatSort) sort: MatSort = new MatSort();
  filterInputControl = new FormControl('');

  constructor(public dialog: MatDialog, private readonly trainingService: TrainingService, private title: Title,
              private readonly notificationService: NotificationService) {
    this.getTraining();
    title.setTitle('Formations');
  }

  ngAfterViewInit(): void {
    this.trainings.sort = this.sort;
  }

  ngOnInit(): void {
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.trainings.filter = filterValue.trim().toLowerCase();
  }

  checkboxLabel(row?: FormationModel): string {
    if (!row) {
      return `${this.isAllSelected() ? 'Sélectionner' : 'Désélectionner'} tout`;
    }
    // @ts-ignore
    return `${this.selectedTrainings.isSelected(row) ? 'Désélectionner' : 'Sélectionner'} ${row.id + 1}`;
  }

  masterToggle(): void {
    this.isAllSelected() ?
      this.selectedTrainings.clear() :
      this.trainings.data.forEach(row => this.selectedTrainings.select(row));
  }

  isAllSelected(): boolean {
    const numSelected = this.selectedTrainings.selected.length;
    const numRows = this.trainings.data.length;
    return numSelected === numRows;
  }

  createOrUpdateTraining(newTraining: FormationModel | null): void {
    const dialogRef = this.dialog.open(PopupCreateUpdateTrainingComponent,
      {
        width: '300px',
        data: {newTraining, trainings: this.trainings.data}
      });
    dialogRef.afterClosed().subscribe(value => {
      this.getTraining();
    });
  }

  deleteTrainings(): void {
    let i = 0;
    this.selectedTrainings.selected.forEach(value => {
      this.trainingService.deleteTraining(value.id as unknown as number).subscribe(value1 => {
          i++;
          if (i === this.selectedTrainings.selected.length) {
            this.getTraining();
          }
        },
        (error) => {
          this.notificationService.error('Erreur lors de suppression de la formation ' + value.nom);
          console.log(error);
        }
      );
    });
  }

  private getTraining(): void {
    this.trainingService.getAllTrainings().subscribe(value => {
      this.trainings.data = value;
      this.selectedTrainings.clear();
      this.trainings.filteredData = value;
      this.filterInputControl.setValue('');
      this.trainings.filter = '';
    });
  }
}
