import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {EnseignementModel} from '../../model/enseignement.model';
import {CreneauModel} from '../../model/creneau.model';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import * as moment from 'moment';
import {AuthService} from '../../core/services/auth.service';
import {CreneauService} from '../../core/services/creneau.service';
import {FormationModel} from '../../model/formation.model';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.component.html',
  styleUrls: ['./liste.component.scss']
})

export class ListeComponent implements AfterViewInit, OnChanges{

  constructor(
    private readonly authService: AuthService,
    private readonly creneauService: CreneauService
  ) {
  }
  @Input() teachers: UtilisateurModel[] = [];
  @Input() courses: EnseignementModel[] = [];
  @Input() classEvents: CreneauModel[] = [];
  @Input() selectedTraining?: FormationModel;
  @Output() presents = new EventEmitter<CreneauModel>();
  @Output() edit = new EventEmitter<CreneauModel>();
  @Output() delete = new EventEmitter<CreneauModel>();

  displayedColumns: string[] = ['cour',  'type', 'teacher', 'horaire', 'date'];
  dynamicColumns: string[] = [];

  dataSource = new MatTableDataSource(this.filterPastEvents(this.classEvents));

  @ViewChild(MatSort) sort: MatSort = new MatSort;


  ngOnChanges(changes: SimpleChanges) {
    this.dataSource.data = this.filterPastEvents(changes.classEvents.currentValue);

    if (this.authService.isAdmin()) { this.dynamicColumns = ['nbInscrits', 'actions']; }
    if (this.authService.isStudent()) { this.dynamicColumns = ['inscription']; }
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'date':
          return moment(item.dateDeb).format('YYYY-MM-DD HH:mm');
        default: // @ts-ignore
          return item[property];
      }
    };
  }
  /**
   * Get the event time
   * @param {CreneauModel} item event
   * @return {string} event time as a string
   */
  getHoraire(item: CreneauModel): string {
    item.dateDeb = new Date(item.dateDeb as Date);
    item.dateFin = new Date(item.dateFin as Date);
    let minutesDeb: any = item.dateDeb?.getMinutes();
    minutesDeb = !minutesDeb || minutesDeb === 0 ? '' : minutesDeb;
    let minutesEnd: any = item.dateFin?.getMinutes();
    minutesEnd = !minutesEnd || minutesEnd === 0 ? '' : minutesEnd;
    return item.dateDeb?.getHours() + 'h' + minutesDeb + ' - ' +
      item.dateFin?.getHours() + 'h' + minutesEnd;
  }

  /**
   * Get the course name
   * @param {number} idCourse id of course
   * @return {string} name of course
   */
  getCourseName(item: CreneauModel): string {
    const course = this.courses.find(c => c.id == item.idEnseignement);
    if (course?.nom) {
      return course.nom;
    }
    return '';
  }

  /**
   *  Get the date  for an event
   * @param {CreneauModel} item event
   * @return {string} date as a string
   */
  getDate(item: CreneauModel): string  {
    return moment(item.dateDeb).format('DD/MM/YYYY');
  }

  /**
   * Get the name of the teacher for an event
   * @param {CreneauModel} item event
   * @return {string} name of the teacher
   */
  getTeacher(item: CreneauModel): string {
    const teacher: UtilisateurModel | undefined = this.teachers.find(t => t.id === item.idEnseignant);
    if (teacher && teacher.nom && teacher.prenom) {
      return UtilisateurModel.getNomPrenom(teacher);
    }
    return ' ';
  }

  /**
   * Handle checkbox changes
   * @param {Boolean} c state of the checkbox
   * @param {CreneauModel} item event
   */
  handleCheckDeclarants(c: Boolean, item: CreneauModel): void {
    if (item.declarants === undefined) {
      return;
    }
    const inclu = item.declarants.includes(this.authService.getUser()?.id as number);

    if (c && !inclu) { // @ts-ignore
      item.declarants.push(this.authService.getUser().id);
    } else if (!c && inclu) {
      // @ts-ignore
      const index = item.declarants.indexOf(this.authService.getUser().id, 0);
      if (index > -1) {
        item.declarants.splice(index, 1);
      }
    }
    this.creneauService.updateCreneau(this.selectedTraining?.id as number, item).subscribe();
  }

  /**
   * Determine if a user is registered for an event
   * @param {CreneauModel} item event
   * @return {boolean} should the item be checked
   */
  checkedDeclarants(item: CreneauModel): boolean {
    if (item.declarants == undefined) {
      item.declarants = [];
    }
    if (this.authService.getUser() !== undefined) { // @ts-ignore
      return item.declarants.includes(this.authService.getUser()?.id as number);
    } else {
      return false;
    }
  }

  /**
   * Is the event before today
   * @param {CreneauModel} item event
   * @return{boolean} Is the event before today
   */
  checkEventDone(item: CreneauModel): boolean {
    return moment().isAfter(moment(item.dateFin));
  }

  /**
   * Filters all the events
   * @param items list of events
   */
  filterPastEvents(items: CreneauModel[]) {
    return items.filter((c) => {
      if (!this.authService.isStudent()) {
        return true;
      }
      if (this.checkedDeclarants(c) || !this.checkEventDone(c)) {
        return true;
      }

      else {
        return false;
      }
    });

  }

  /**
   * Event for editing an event
   * @param {CreneauModel} item event
   */
  editClassEvent(item: CreneauModel): void {
    this.edit.emit(item);
  }
  /**
   * Event for editing the users declared for an event
   * @param {CreneauModel} item event
   */
  presentsClassEvent(item: CreneauModel): void {
    this.presents.emit(item);
  }

  /**
   * Event for deleting an event
   * @param {CreneauModel} item event
   */
  deleteModale(item: CreneauModel): void  {
    if (item) {
      this.delete.emit(item);
    }
  }

  /**
   * Get the number of student registered
   * @param {CreneauModel} item event
   * @returns {number} the number of student registered
   */
  getNbInscrit(item: CreneauModel): number{
    return item.declarants ? item.declarants.length : 0;
  }


}
