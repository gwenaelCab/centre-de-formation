import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormationModel} from '../../../model/formation.model';
import {TrainingService} from '../../../core/services/training.service';
import {NotificationService} from '../../../core/services/notification.service';
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-popup-create-update-training',
  templateUrl: './popup-create-update-training.component.html',
  styleUrls: ['./popup-create-update-training.component.scss']
})
export class PopupCreateUpdateTrainingComponent implements OnInit {
  training: FormationModel = new FormationModel(undefined, '', []);
  name: FormControl = new FormControl('', Validators.minLength(1));

  constructor(public dialogRef: MatDialogRef<PopupCreateUpdateTrainingComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private readonly trainingService: TrainingService,
              private readonly notificationService: NotificationService) {
    if (data.training != null) {
      this.training = data.training;
    }
    this.name.setValue(this.training.nom);
    this.name.valueChanges.subscribe(value => this.training.nom = value);
  }

  ngOnInit(): void {
  }

  isNameEmpty(): boolean {
    return this.training.nom === undefined || this.training.nom.trim().length === 0;
  }

  nameIsAlreadyTaken(): boolean {
    return (this.data.trainings as []).some((t: FormationModel) => {
      return t.id !== this.training.id && t.nom?.trim()?.toLocaleUpperCase() === this.training.nom?.toLocaleUpperCase();
    });
  }

  onSubmit(): void {
    if (!this.isNameEmpty() && !this.nameIsAlreadyTaken()) {
      this.trainingService.insertTraining(this.training).subscribe(
        () => {
        },
        (error) => {
          this.notificationService.error();
          this.dialogRef.close(false);
          console.log(error);
        },
        () => this.dialogRef.close(true));
    }
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

  updateName($event: any): void {
    this.training.nom = ($event.target as HTMLInputElement).value;
  }
}
