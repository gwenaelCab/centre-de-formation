import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PopupCreateUpdateTrainingComponent} from './popup-create-update-training.component';

describe('PopupCreateUpdateTrainingComponent', () => {
  let component: PopupCreateUpdateTrainingComponent;
  let fixture: ComponentFixture<PopupCreateUpdateTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PopupCreateUpdateTrainingComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupCreateUpdateTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
