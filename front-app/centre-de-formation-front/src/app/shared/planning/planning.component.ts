import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {UtilisateurModel} from '../../model/utilisateur.model';
import {EnseignementModel} from '../../model/enseignement.model';
import {CreneauModel} from '../../model/creneau.model';
import * as moment from 'moment';
import {Moment} from 'moment';
import {AuthService} from '../../core/services/auth.service';

@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit {

  @Input() teachers: UtilisateurModel[] = [];
  @Input() courses: EnseignementModel[] = [];
  coursesOfTheWeek: [{ name: string; classEvents: any[] }, { name: string; classEvents: any[] },
    { name: string; classEvents: any[] }, { name: string; classEvents: any[] },
    { name: string; classEvents: any[] }, { name: string; classEvents: any[] }] =
    [
      {name: 'Lun.', classEvents: []},
      {name: 'Mar.', classEvents: []},
      {name: 'Mer.', classEvents: []},
      {name: 'Jeu.', classEvents: []},
      {name: 'Ven.', classEvents: []},
      {name: 'Sam.', classEvents: []},
    ];

  @Output() selectedWeekChange = new EventEmitter<Moment>();
  @Output() presents = new EventEmitter<CreneauModel>();
  @Output() edit = new EventEmitter<CreneauModel>();
  private privateSelectedWeek: Moment = moment();
  private allClassEvents: CreneauModel[] = [];
  private classEventsOfTheWeek: CreneauModel[] = [];

  constructor(public authService: AuthService) {
  }

  private privateWeekNumber = -1;
  hoverElement?: HTMLElement;

  get weekNumber(): number {
    return this.privateWeekNumber;
  }

  get selectedWeek(): Moment {
    return this.privateSelectedWeek;
  }

  @Input()
  set selectedWeek(selectedWeek: Moment) {
    this.privateSelectedWeek = selectedWeek;
    this.privateWeekNumber = selectedWeek.week();
    this.updateCoursesOfTheWeek();
  }

  get classEvents(): CreneauModel[] {
    return this.allClassEvents;
  }

  @Input()
  set classEvents(val: CreneauModel[]) {
    this.allClassEvents = val;
    this.updateCoursesOfTheWeek();
  }

  ngOnInit(): void {
  }

  /**
   * @param dayNumber from 0 to 5
   */
  getClassEventsOfDay(dayNumber: number): CreneauModel[] {
    // @ts-ignore
    const classEventsOfTheWeek = this.classEventsOfTheWeek.filter(classEvent => moment(classEvent.dateDeb).isoWeekday() - 1 === dayNumber);
    // @ts-ignore
    return classEventsOfTheWeek.sort((a, b) => {
      const dateA = moment(a?.dateDeb as Date).format('HHmm') as unknown as number;
      const dateB = moment(b?.dateDeb as Date).format('HHmm') as unknown as number;
      return dateA - dateB;
    });
  }

  /**
   * event on classEvent drop
   * @param event on drop
   */
  drop(event: CdkDragDrop<CreneauModel[]>): void {

    // TODO vérifié les régles metiers : profs libre, pas de chevauchement ....
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  /**
   * get first line to display for a classEvent
   * @param item classEvent
   */
  getLine1(item: CreneauModel): string {
    item.dateDeb = new Date(item.dateDeb as Date);
    item.dateFin = new Date(item.dateFin as Date);
    let minutesDeb: any = item.dateDeb?.getMinutes();
    minutesDeb = !minutesDeb || minutesDeb === 0 ? '' : minutesDeb;
    let minutesEnd: any = item.dateFin?.getMinutes();
    minutesEnd = !minutesEnd || minutesEnd === 0 ? '' : minutesEnd;
    return item.dateDeb?.getHours() + 'h' + minutesDeb + ' - ' +
      item.dateFin?.getHours() + 'h' + minutesEnd;
  }

  /**
   * get second line to display for a classEvent
   * @param item classEvent
   */
  getLine2(item: CreneauModel): string {
    const courseName = this.courses.find(c => c.id === item.idEnseignement)?.nom;
    return courseName + ' - ' + item.type + ' - ' + item.modalite;
  }

  /**
   * get color according to the class subject
   * @param item classEvent
   */
  getColor(item: CreneauModel): string | undefined {
    return this.courses.find(c => c.id === item.idEnseignement)?.color;
  }

  /**
   * get third line to display for a classEvent
   * @param item classEvent
   */
  getLine3(item: CreneauModel): string {
    const teacher: UtilisateurModel | undefined = this.teachers.find(t => t.id === item.idEnseignant);
    if (teacher && teacher.nom && teacher.prenom) {
      return UtilisateurModel.getNomPrenom(teacher);
    }
    return ' ';
  }

  /**
   * get fourth line to display for a classEvent
   * @param item classEvent
   */
  getLine4(item: CreneauModel): string {
    if (this.authService.isStudent()) {
      const inscrit = item.declarants?.includes(this.authService.getUser().id as number) ? 'Incrit' : 'Non inscrit';
      const present = item.presents?.includes(this.authService.getUser().id as number) ? 'Présent' : 'Absent';
      const classIsDone = moment(item.dateFin).isBefore(moment());
      if (inscrit === 'Incrit' && classIsDone) {
        return present;
      } else {
        return inscrit;
      }
    } else {
      const n = item.declarants ? item.declarants.length : 0;
      return n + ' inscrit' + (n > 1 ? 's' : '');
    }
  }

  /**
   * date formater
   */
  getMonthYear(): string {
    return this.selectedWeek.format('MMMM') + ' ' + this.selectedWeek.year();
  }

  /**
   * go to today / this week
   */
  today(): void {
    this.selectedWeek = moment();
    this.selectedWeekChange.emit(this.selectedWeek);
  }

  /**
   * upadate displated data
   * @param n number in year of the week
   */
  weekChange(n: number): void {
    this.selectedWeek = this.selectedWeek.add(n, 'w');
    this.selectedWeekChange.emit(this.selectedWeek);
  }

  getDay(n: number): number {
    const m = moment().year(this.selectedWeek.year()).week(this.selectedWeek.week()).startOf('week');
    return m.add(n, 'days').date();
  }

  /**
   * edit class event
   * @param item clicked
   */
  editClassEvent(item: CreneauModel): void {
    this.edit.emit(item);
  }

  /**
   * presents class event
   * @param item clicked
   */
  presentsClassEvent(item: CreneauModel): void {
    this.presents.emit(item);
  }

  /**
   * reload call event of the week
   * get classevent for whicj=h day
   * @private
   */
  private updateCoursesOfTheWeek(): void {
    this.classEventsOfTheWeek = this.allClassEvents.filter(classEvent =>
      moment(classEvent.dateDeb).week() === this.selectedWeek.week());
    this.coursesOfTheWeek.forEach((item, index) => {
      item.classEvents = this.getClassEventsOfDay(index);
    });

  }
}
