import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanningComponent} from './planning/planning.component';
import {ListeComponent} from "./liste/liste.component";
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SharedModule} from './shared/shared.module';
import {PopupCreateUpdateTrainingComponent} from './planning/popup-create-update-training/popup-create-update-training.component';
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [PlanningComponent, PopupCreateUpdateTrainingComponent, ListeComponent],
  exports: [
    PlanningComponent, ListeComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    DragDropModule,
    MatIconModule,
  ]
})
export class PlanningModule {
}
