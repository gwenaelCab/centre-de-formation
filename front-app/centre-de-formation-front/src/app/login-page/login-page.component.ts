import {Component, OnInit} from '@angular/core';
import {AuthService} from "../core/services/auth.service";
import {Router} from "@angular/router";
import {UserService} from "../core/services/user.service";
import {UtilisateurModel} from "../model/utilisateur.model";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  selectedUserId?: number;
  users: UtilisateurModel[] = [];

  constructor(public authService: AuthService, private router: Router, public userService: UserService) {
    this.ifLoggedRedirectToPlanning();
  }

  /**
   * if user is already login redirect to /
   */
  ifLoggedRedirectToPlanning(): void {
    if (this.authService.isLogin()) {
      this.router.navigate(['']);
    }
  }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe(value => {
      this.users = value;
      this.selectedUserId = value[0].id;
    });
  }

  /**
   * try to login
   */
  connection(): void {
    // @ts-ignore
    this.authService.login(this.users.find((value: UtilisateurModel) => value.id === this.selectedUserId) as UtilisateurModel);
  }
}
