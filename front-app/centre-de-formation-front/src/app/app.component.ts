import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public title: Title) {
    moment.locale('fr');
  }


}
