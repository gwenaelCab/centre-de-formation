var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var utilisateurRouter = require('./routes/utilisateur');
var enseignementRouter = require('./routes/enseignement');
var formationRouter = require('./routes/formation');


const allowedExt = [
    '.js',
    '.ico',
    '.css',
    '.png',
    '.jpg',
    '.woff2',
    '.woff',
    '.ttf',
    '.svg',
];

var app = express();

var api = '/api'
var front_dist_path = __dirname + '/front-app/centre-de-formation-front/dist/centre-de-formation-front/'

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// ENV spce
if (process.env.env === 'production') {
    console.log("production env detected")
    app.use(logger('production'));
} else {
    console.log("No production env detected, CORS Allowed,")
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "*",)
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.use(logger('dev'));
}

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());

app.use(api + '/', indexRouter);
app.use(api + '/utilisateur', utilisateurRouter);
app.use(api + '/enseignement', enseignementRouter);
app.use(api + '/formation', formationRouter);

const unless = (pathExcluded, middleware) => {
    return function (req, res, next) {
        req.path.toString().startsWith(pathExcluded) ? next() : middleware(req, res, next);
    };
};

// Redirect all the front requests
app.use(unless('/api/', (req, res, next) => {
    if (allowedExt.filter(ext => req.url.includes(ext)).length > 0) {
        res.sendFile(path.resolve(front_dist_path + req.url));
    } else {
        res.sendFile(front_dist_path + 'index.html');
    }
}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    var emoji = '👀';
    switch (err.status) {
        case 404:
            emoji = '🙈';
            break;
        case 500:
            emoji = '💥';
            break;
    }
    res.locals.emoji = emoji;
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

console.log("" +
    "\n" +
    "                 (  .      )\n" +
    "             )           (              )\n" +
    "                   .  '   .   '  .  '  .\n" +
    "          (    , )       (.   )  (   ',    )\n" +
    "           .' ) ( . )    ,  ( ,     )   ( .\n" +
    "        ). , ( .   (  ) ( , ')  .' (  ,    )\n" +
    "       (_,) . ), ) _) _,')  (, ) '. )  ,. (' )\n" +
    "      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ \n" +
    " Server is Running and accessible http://localhost:3000\n");


module.exports = app;
